//
// Created by rsushe on 3/1/23.
//
/// @file
/// @brief Header file describing the structure of PE header
#ifndef SECTION_EXTRACTOR_PEHEADER_H
#define SECTION_EXTRACTOR_PEHEADER_H

#include <stdint.h>

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

/// @brief Structure contains COFF File Header
struct
#if defined(__GNUC__) || defined(_MSC_VER)
        __attribute__((packed))
#endif
PEHeader {

    /// The number that identifies the type of target machine
    uint16_t machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t number_of_sections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970, which indicates when the file was created
    uint32_t time_data_stamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    uint32_t pointer_to_symbol_table;
    /// The number of entries in the symbol table
    uint32_t number_of_symbols;
    /// The size of the optional header, which is required for executable files but not for object files
    uint16_t size_of_optional_header;
    /// The flags that indicate the attributes of the file
    uint16_t characteristics;

};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

#endif //SECTION_EXTRACTOR_PEHEADER_H
