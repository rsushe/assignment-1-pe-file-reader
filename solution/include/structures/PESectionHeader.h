//
// Created by rsushe on 3/1/23.
//
/// @file
/// @brief Header file describing the structure of PE Section Header
#ifndef SECTION_EXTRACTOR_PESECTIONHEADER_H
#define SECTION_EXTRACTOR_PESECTIONHEADER_H

#include <stdint.h>

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

/// @brief Structure contains Section Header
struct
#if defined(__GNUC__) || defined(_MSC_VER)
        __attribute__((packed))
#endif
PESectionHeader {
    /// Name of section header
    char name[8];
    /// The total size of the section when loaded into memory
    uint32_t virtual_size;
    /// Arbitrary value that is subtracted from offsets during relocation
    uint32_t virtual_address;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    uint32_t size_of_row_data;
    /// The file pointer to the first page of the section within the COFF file
    uint32_t pointer_to_row_data;
    /// The file pointer to the beginning of relocation entries for the section
    uint32_t pointer_to_relocations;
    /// The file pointer to the beginning of line-number entries for the section
    uint32_t pointer_to_linenumbers;
    /// The number of relocation entries for the section
    uint16_t number_of_relocations;
    /// The number of line-number entries for the section
    uint16_t number_of_linenumbers;
    /// The flags that describe the characteristics of the section
    uint32_t characteristics;

};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

#endif //SECTION_EXTRACTOR_PESECTIONHEADER_H
