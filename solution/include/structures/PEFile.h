//
// Created by rsushe on 2/18/23.
//
/// @file
/// @brief Header file describing the structure of PE file and common function for working with it
#ifndef SECTION_EXTRACTOR_PEFILE_H
#define SECTION_EXTRACTOR_PEFILE_H

#include "PEHeader.h"
#include "PESectionHeader.h"
#include "enum/process_status.h"
#include <malloc.h>
#include <stdint.h>

/// Size of the signature at the beginning of the file
#define PE_SIGNATURE_SIZE 4

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

/// @brief Structure contains offsets to main file section and headers itself
struct
#if defined(__GNUC__) || defined(_MSC_VER)
        __attribute__((packed))
#endif
PEFile {

    /// @name Offsets within file
    ///@{

    /// Offset to a file magic
    uint32_t magic_offset;
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Offset to an optional header
    uint32_t optional_header_offset;
    /// Offset to a section table
    uint32_t section_header_offset;
    ///@}

    /// @name File headers
    ///@{

    /// File magic
    uint32_t magic;
    /// Main header
    struct PEHeader header;
    /// Array of section headers with the size of header.number_of_sections
    struct PESectionHeader *section_headers;
    ///@}

};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Function for reading pe file into pe file structure
/// @param[in] in_file Input file
/// @param[in] peFile Pointer to structure to be filled
/// @return SUCCESS in case of success reading or ERROR else
enum status read_PEFile_struct(FILE *in_file, struct PEFile *peFile);

/// @brief Function for finding section in pe file structure
/// @param[in] peFile Pointer to structure
/// @param[in] section_header_name Name to be found
/// @param[in] size_of_raw_data Pointer to size to be set in case of success search
/// @param[in] pointer_to_raw_data Pointer to pointer to be set in case of success search
/// @return SUCCESS in case of success search or ERROR else
enum status
find_section_header_with_name(struct PEFile *peFile, char *section_header_name, uint32_t *size_of_raw_data,
                              uint32_t *pointer_to_raw_data);

#endif //SECTION_EXTRACTOR_PEFILE_H
