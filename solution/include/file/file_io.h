//
// Created by rsushe on 2/28/23.
//
/// @file
/// @brief Module for working with file
#ifndef SECTION_EXTRACTOR_FILE_IO_H
#define SECTION_EXTRACTOR_FILE_IO_H

#include "enum/process_status.h"
#include <stdint.h>
#include <stdio.h>

/// @brief Function for file opening
/// @param[in] file Input file to be set
/// @param[in] file_name Name of input file
/// @param[in] read_mode Mode in which open file
/// @return SUCCESS in case of success opening or ERROR else
enum status open_file(FILE** file, const char* file_name, const char* read_mode);

/// @brief Function for file closing
/// @param[in] file Input file
/// @return SUCCESS in case of success closing or ERROR else
enum status close_file(FILE* file);

/// @brief Function for reading data from file
/// @param[in] file Input file
/// @param[in] to_read Pointer to data to be set in case of success reading
/// @param[in] sizeof_to_read Size of data to read
/// @param[in] number_of_data_to_read count of block of data to read
/// @return SUCCESS in case of success reading or ERROR else
enum status read_from_file(FILE* file, uint8_t* to_read, size_t sizeof_to_read, size_t number_of_data_to_read);

/// @brief Function for writing data from file
/// @param[in] file Input file
/// @param[in] to_read Pointer to data to be write
/// @param[in] sizeof_to_read Size of data to write
/// @param[in] number_of_data_to_read count of block of data to write
/// @return SUCCESS in case of success writing or ERROR else
enum status write_to_file(FILE* file, uint8_t* to_write, size_t sizeof_to_write, size_t number_of_data_to_write);

#endif //SECTION_EXTRACTOR_FILE_IO_H
