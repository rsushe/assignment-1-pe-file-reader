//
// Created by rsushe on 3/2/23.
//
/// @file
/// @brief Module describing enum of statuses
#ifndef SECTION_EXTRACTOR_PROCESS_STATUS_H
#define SECTION_EXTRACTOR_PROCESS_STATUS_H

enum status {
    SUCCESS,
    ERROR
};

#endif //SECTION_EXTRACTOR_PROCESS_STATUS_H
