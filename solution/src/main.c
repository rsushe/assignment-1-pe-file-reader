/// @file 
/// @brief Main application file

#include "file/file_io.h"
#include "structures/PEFile.h"
#include <stdint.h>
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"
/// Code to return when error occurs
#define ERROR_CODE 1

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f) {
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Print error message to stderr
/// @param[in] error_message message to print
void print_error(char *error_message) {
    fprintf(stderr, "%s\n", error_message);
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char **argv) {
    if (argc != 4) {
        usage(stderr);
        return ERROR_CODE;
    }

    char *in_file_name = argv[1], *section_name = argv[2], *out_file_name = argv[3];

    FILE *in_file, *out_file;

    enum status open_status = open_file(&in_file, in_file_name, "rb");
    if (open_status == ERROR) {
        print_error("in file doesn't exist");
        return ERROR_CODE;
    }

    open_status = open_file(&out_file, out_file_name, "wb");
    if (open_status == ERROR) {
        close_file(in_file);
        print_error("out file doesn't exist");
        return ERROR_CODE;
    }

    struct PEFile peFile;

    enum status read_PEFile_status = read_PEFile_struct(in_file, &peFile);
    if (read_PEFile_status == ERROR) {
        close_file(in_file);
        close_file(out_file);
        print_error("error while reading PE file struct from file");
        return ERROR_CODE;
    }

    uint32_t size_of_raw_data, pointer_to_raw_data;

    enum status section_header_find_status = find_section_header_with_name(&peFile, section_name, &size_of_raw_data,
                                                                           &pointer_to_raw_data);

    if (section_header_find_status == ERROR) {
        free(peFile.section_headers);
        close_file(in_file);
        close_file(out_file);
        print_error("Section header with given name doesn't exist");
        return ERROR_CODE;
    }

    fseek(in_file, pointer_to_raw_data, SEEK_SET);

    uint8_t *section_data = malloc(sizeof(uint8_t) * size_of_raw_data);

    printf("%lu %lu\n", sizeof(uint8_t), sizeof(section_data));

    enum status read_from_file_status = read_from_file(in_file, section_data, sizeof(uint8_t), size_of_raw_data);
    if (read_from_file_status == ERROR) {
        free(section_data);
        free(peFile.section_headers);
        close_file(in_file);
        close_file(out_file);
        print_error("Error when reading section data from input file");
        return ERROR_CODE;
    }

    enum status write_to_file_status = write_to_file(out_file, section_data, sizeof(uint8_t), size_of_raw_data);
    if (write_to_file_status == ERROR) {
        free(section_data);
        free(peFile.section_headers);
        close_file(in_file);
        close_file(out_file);
        print_error("Error when writing section data to output file");
        return ERROR_CODE;
    }

    free(section_data);
    free(peFile.section_headers);
    close_file(in_file);
    close_file(out_file);

    return 0;
}
