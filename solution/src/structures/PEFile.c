//
// Created by rsushe on 3/2/23.
//
/// @file
/// @brief Class file for function implementations
#include "structures/PEFile.h"
#include <stdio.h>
#include <string.h>

/// @brief Function for reading pe file into pe file structure
/// @param[in] in_file Input file
/// @param[in] peFile Pointer to structure to be filled
/// @return SUCCESS in case of success reading or ERROR else
enum status read_PEFile_struct(FILE *in_file, struct PEFile *peFile) {
    if (fseek(in_file, 0x3c, SEEK_SET) != 0) {
        return ERROR;
    }

    uint32_t offset;
    if (!fread(&offset, sizeof(uint32_t), 1, in_file)) {
        return ERROR;
    }

    peFile->magic_offset = offset;
    peFile->header_offset = peFile->magic_offset + PE_SIGNATURE_SIZE;

    if (fseek(in_file, peFile->magic_offset, SEEK_SET) != 0) {
        return ERROR;
    }

    if (!fread(&peFile->magic, sizeof(peFile->magic), 1, in_file)) {
        return ERROR;
    }

    if (!fread(&peFile->header, sizeof(peFile->header), 1, in_file)) {
        return ERROR;
    }

    uint16_t number_of_sections = peFile->header.number_of_sections;

    peFile->optional_header_offset = peFile->header_offset + sizeof(struct PEHeader);
    peFile->section_header_offset = peFile->optional_header_offset + peFile->header.size_of_optional_header;
    peFile->section_headers = malloc(sizeof(struct PESectionHeader) * number_of_sections);

    if (fseek(in_file, peFile->section_header_offset, SEEK_SET) != 0) {
        free(peFile->section_headers);
        return ERROR;
    }

    if (fread(peFile->section_headers, sizeof(struct PESectionHeader), number_of_sections, in_file) !=
        number_of_sections) {
        free(peFile->section_headers);
        return ERROR;
    }

    return SUCCESS;
}

/// @brief Function for finding section in pe file structure
/// @param[in] peFile Pointer to structure
/// @param[in] section_header_name Name to be found
/// @param[in] size_of_raw_data Pointer to size to be set in case of success search
/// @param[in] pointer_to_raw_data Pointer to pointer to be set in case of success search
/// @return SUCCESS in case of success search or ERROR else
enum status find_section_header_with_name(struct PEFile *peFile, char *section_header_name, uint32_t *size_of_raw_data,
                                          uint32_t *pointer_to_raw_data) {
    for (uint16_t i = 0; i < peFile->header.number_of_sections; i++) {
        if (strcmp(peFile->section_headers[i].name, section_header_name) == 0) {
            *size_of_raw_data = peFile->section_headers[i].size_of_row_data;
            *pointer_to_raw_data = peFile->section_headers[i].pointer_to_row_data;
            return SUCCESS;
        }
    }
    return ERROR;
}
